/*
 * Import Actions {...}
 * ******************** */
import * as Actions from "../actions/ActionsTypes.js";

/*
 * Selector
 * ******** */
const initialState = {
    dataImagesArticle: [],
};

/*
 * Reducers
 * ******** */

export function ImagesArticleReducer(state = initialState, action) {
    switch (action.type) {
        default:
            return state;
        case Actions.GET_IMAGESARTICLES_DATA:
            return { ...state, dataImagesArticle: action.payload };
        case Actions.POST_IMAGESARTICLE_DATA:
            return { ...state, dataImagesArticle: action.payload };
        case Actions.DELETEONE_IMAGEARTICLE_DATA:
            return { ...state, dataImagesArticle: action.payload };
        case Actions.DELETEALL_IMAGESARTICLES_DATA:
            return { ...state, dataImagesArticle: action.payload };
        case Actions.DELETE_IMAGESPERARTICLES_DATA:
            return { ...state, dataImagesArticle: action.payload };
    }
}

/*
 * Getters
 * ******* */
