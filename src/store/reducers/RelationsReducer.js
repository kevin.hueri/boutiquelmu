/*
 * Import Actions {...}
 * ******************** */
import * as Actions from "../actions/ActionsTypes.js";

/*
 * Selector
 * ******** */
const initialState = {
    dataRelations: [],
    dataRelationsCommandeArticles: []
};

/*
 * Reducers
 * ******** */

export function RelationsReducer(state = initialState, action) {
    switch (action.type) {
        default:
            return state;
        case Actions.GET_IMAGESANDARTICLES_DATA:
            return { ...state, dataRelations: action.payload };
        case Actions.GET_IMAGESANDARTICLE_DATA:
            return { ...state, dataRelations: action.payload };
        case Actions.GET_ARTICLESPERCOMMANDE_DATA:
            return { ...state, dataRelationsCommandeArticles: action.payload };
        case Actions.GET_ARTICLESPERCOMMANDES_DATA:
            return { ...state, dataRelationsCommandeArticles: action.payload };
    }
}

/*
 * Getters
 * ******* */
