/*
 * Import Actions {...}
 * ******************** */
import * as Actions from "../actions/ActionsTypes.js";

/*
 * Selector
 * ******** */
const initialState = {
    dataArticles: [],
};

/*
 * Reducers
 * ******** */

export function ArticlesReducer(state = initialState, action) {
    switch (action.type) {
        default:
            return state;
        case Actions.GET_ARTICLES_DATA:
            return { ...state, dataArticles: action.payload };
        case Actions.GETONE_ARTICLES_DATA:
            return { ...state, dataArticles: action.payload };
        case Actions.POST_ARTICLES_DATA:
            return { ...state, dataArticles: action.payload };
        case Actions.PUT_ARTICLES_DATA:
            return { ...state, dataArticles: action.payload };
        case Actions.DELETEONE_ARTICLES_DATA:
            return { ...state, dataArticles: action.payload };
        case Actions.DELETEALL_ARTICLES_DATA:
            return { ...state, dataArticles: action.payload };
    }
}

/*
 * Getters
 * ******* */
