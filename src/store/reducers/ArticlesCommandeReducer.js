/*
 * Import Actions {...}
 * ******************** */
import * as Actions from "../actions/ActionsTypes.js";

/*
 * Selector
 * ******** */
const initialState = {
    dataArticlesCommande: [],
};

/*
 * Reducers
 * ******** */

export function ArticlesCommandeReducer(state = initialState, action) {
    switch (action.type) {
        default:
            return state;
        case Actions.GET_ARTICLESCOMMANDE_DATA:
            return { ...state, dataArticlesCommande: action.payload };
        case Actions.GETONE_ARTICLESCOMMANDE_DATA:
            return { ...state, dataArticlesCommande: action.payload };
        case Actions.POST_ARTICLESCOMMANDE_DATA:
            return { ...state, dataArticlesCommande: action.payload };
        case Actions.PUT_ARTICLESCOMMANDE_DATA:
            return { ...state, dataArticlesCommande: action.payload };
        case Actions.DELETEONE_ARTICLESCOMMANDE_DATA:
            return { ...state, dataArticlesCommande: action.payload };
        case Actions.DELETE_ARTICLESCOMMANDE_DATA:
            return { ...state, dataArticlesCommande: action.payload };
    }
}

/*
 * Getters
 * ******* */
