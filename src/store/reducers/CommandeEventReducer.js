/*
 * Import Actions {...}
 * ******************** */
import * as Actions from "../actions/ActionsTypes.js";

/*
 * Selector
 * ******** */
const initialState = {
    dataCommandeEvent: [],
};

/*
 * Reducers
 * ******** */

export function CommandeEventReducer(state = initialState, action) {
    switch (action.type) {
        default:
            return state;
        case Actions.GET_COMMANDEEVENT_DATA:
            return { ...state, dataCommandeEvent: action.payload };
        case Actions.GETONE_COMMANDEEVENT_DATA:
            return { ...state, dataCommandeEvent: action.payload };
        case Actions.POST_COMMANDEEVENT_DATA:
            return { ...state, dataCommandeEvent: action.payload };
        case Actions.PUT_COMMANDEEVENT_DATA:
            return { ...state, dataCommandeEvent: action.payload };
        case Actions.DELETEONE_COMMANDEEVENT_DATA:
            return { ...state, dataCommandeEvent: action.payload };
        case Actions.DELETE_COMMANDEEVENT_DATA:
            return { ...state, dataCommandeEvent: action.payload };
    }
}

/*
 * Getters
 * ******* */
