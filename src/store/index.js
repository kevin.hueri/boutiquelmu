/*
 * Import config store by react
 * **************************** */
import { createStore, combineReducers, applyMiddleware } from "redux";
import { composeWithDevTools } from "redux-devtools-extension";
import thunk from "redux-thunk";

/*
 * Reducers
 * ******** */
import { ArticlesReducer } from "./reducers/ArticlesReducer";
import { ImagesArticleReducer } from "./reducers/ImagesArticleReducer";
import { RelationsReducer } from "./reducers/RelationsReducer";
import { ArticlesCommandeReducer } from "./reducers/ArticlesCommandeReducer";
import { CommandeEventReducer } from "./reducers/CommandeEventReducer";

/*
 * Centralisation du store (root reducers)
 * *************************************** */
const rootReducer = combineReducers({
    article: ArticlesReducer,
    imagesArticle: ImagesArticleReducer,
    relations: RelationsReducer,
    articleCommande: ArticlesCommandeReducer,
    commandeEvent: CommandeEventReducer
});

/*
 * Export du store
 * *************** */
export const store = createStore(rootReducer, composeWithDevTools(applyMiddleware(thunk))); //Dev
// export const store = createStore(rootReducer); //prod

