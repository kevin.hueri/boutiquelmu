/*
 * Import - Module
 * *************** */
import { apiTemplate } from "../../config/axios";

/*
 * Import types {...}
 * ****************** */
import {
    GET_ARTICLESCOMMANDE_DATA,
    GETONE_ARTICLESCOMMANDE_DATA,
    POST_ARTICLESCOMMANDE_DATA,
    PUT_ARTICLESCOMMANDE_DATA,
    DELETEONE_ARTICLESCOMMANDE_DATA,
    DELETE_ARTICLESCOMMANDE_DATA,
} from "./ActionsTypes.js";

/*
* Actions
* ******* */

// Get articles commande
export const getAllArticleCommande = () => {
    return (dispatch) => {
        return apiTemplate
            .get("/articles-commande")
            .then((res) => { dispatch({ type: GET_ARTICLESCOMMANDE_DATA, payload: res.data.dbArticleCommande }); })
            .catch((err) => console.log(err));
    };
};

// GetOne articles commande
export const getOneArticleCommande = (id) => {
    return (dispatch) => {
        return apiTemplate
            .get(`/articles-commande/${id}`)
            .then((res) => { dispatch({ type: GETONE_ARTICLESCOMMANDE_DATA, payload: res.data.dbArticleCommande }); })
            .catch((err) => console.log(err));
    };
};

// Post articles commande
export const postArticleCommande = (id, data) => {
    return (dispatch) => {
        return apiTemplate
            .post(`/articles-commande/${id}`, data)
            .then((res) => { dispatch({ type: POST_ARTICLESCOMMANDE_DATA, payload: res.data.dbArticleCommande }); })
            .catch((err) => console.log(err));
    };
};

// Put articles commande
export const putArticleCommande = (id, data) => {
    return (dispatch) => {
        return apiTemplate
            .put(`/articles-commande/${id}`, data)
            .then((res) => { dispatch({ type: PUT_ARTICLESCOMMANDE_DATA, payload: res.data.dbArticleCommande }); })
            .catch((err) => console.log(err));
    };
};

// DeleteOne articles commande
export const deleteOneArticleCommande = (id) => {
    return (dispatch) => {
        return apiTemplate
            .delete(`/articles-commande/${id}`)
            .then((res) => {
                dispatch({ type: DELETEONE_ARTICLESCOMMANDE_DATA, payload: res.data.dbArticleCommande });
            })
            .catch((err) => console.log(err));
    };
};

// DeleteAll articles commande
export const deleteAllArticleCommande = () => {
    return (dispatch) => {
        return apiTemplate
            .delete("/articles-commande")
            .then((res) => {
                dispatch({ type: DELETE_ARTICLESCOMMANDE_DATA, payload: res.data.dbArticleCommande });
            })
            .catch((err) => console.log(err));
    };
};
