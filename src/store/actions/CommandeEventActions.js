/*
 * Import - Module
 * *************** */
import { apiTemplate } from "../../config/axios";

/*
 * Import types {...}
 * ****************** */
import {
    GET_COMMANDEEVENT_DATA,
    GETONE_COMMANDEEVENT_DATA,
    POST_COMMANDEEVENT_DATA,
    PUT_COMMANDEEVENT_DATA,
    DELETEONE_COMMANDEEVENT_DATA,
    DELETE_COMMANDEEVENT_DATA,
} from "./ActionsTypes.js";

/*
* Actions
* ******* */

// Get evenement de commande
export const getAllCommandeEvent = () => {
    return (dispatch) => {
        return apiTemplate
            .get("/commande-event")
            .then((res) => { dispatch({ type: GET_COMMANDEEVENT_DATA, payload: res.data.dbCommandeEvent }); })
            .catch((err) => console.log(err));
    };
};

// GetOne evenement de commande
export const getOneCommandeEvent = (id) => {
    return (dispatch) => {
        return apiTemplate
            .get(`/commande-event/${id}`)
            .then((res) => { dispatch({ type: GETONE_COMMANDEEVENT_DATA, payload: res.data.dbCommandeEvent }); })
            .catch((err) => console.log(err));
    };
};

// Post evenement de commande
export const postCommandeEvent = (data) => {
    return (dispatch) => {
        return apiTemplate
            .post(`/commande-event`, data)
            .then((res) => { dispatch({ type: POST_COMMANDEEVENT_DATA, payload: res.data.dbCommandeEvent }); })
            .catch((err) => console.log(err));
    };
};

// Put evenement de commande
export const putCommandeEvent = (id, data) => {
    return (dispatch) => {
        return apiTemplate
            .put(`/commande-event/${id}`, data)
            .then((res) => { dispatch({ type: PUT_COMMANDEEVENT_DATA, payload: res.data.dbCommandeEvent }); })
            .catch((err) => console.log(err));
    };
};

// DeleteOne evenement de commande
export const deleteOneCommandeEvent = (id) => {
    return (dispatch) => {
        return apiTemplate
            .delete(`/commande-event/${id}`)
            .then((res) => {
                dispatch({ type: DELETEONE_COMMANDEEVENT_DATA, payload: res.data.dbCommandeEvent });
            })
            .catch((err) => console.log(err));
    };
};

// DeleteAll evenement de commande
export const deleteAllCommandeEvent = () => {
    return (dispatch) => {
        return apiTemplate
            .delete("/commande-event")
            .then((res) => {
                dispatch({ type: DELETE_COMMANDEEVENT_DATA, payload: res.data.dbCommandeEvent });
            })
            .catch((err) => console.log(err));
    };
};
