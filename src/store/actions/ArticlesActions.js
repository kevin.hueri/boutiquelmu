/*
 * Import - Module
 * *************** */
import { apiTemplate } from "../../config/axios";

/*
 * Import types {...}
 * ****************** */
import {
    GET_ARTICLES_DATA,
    GETONE_ARTICLES_DATA,
    POST_ARTICLES_DATA,
    PUT_ARTICLES_DATA,
    DELETEONE_ARTICLES_DATA,
    DELETEALL_ARTICLES_DATA,
} from "./ActionsTypes.js";

/*
* Actions
* ******* */

// Get articles
export const getAllArticles = () => {
    return (dispatch) => {
        return apiTemplate
            .get("/articles")
            .then((res) => {
                dispatch({ type: GET_ARTICLES_DATA, payload: res.data.dbarticles });
            })
            .catch((err) => console.log(err));
    };
};

// GetOne article
export const getOneArticle = (id) => {
    return (dispatch) => {
        return apiTemplate
            .get(`/articles/${id}`)
            .then((res) => {
                dispatch({ type: GETONE_ARTICLES_DATA, payload: res.data.dbarticles });
            })
            .catch((err) => console.log(err));
    };
};

// Post article
export const postArticle = (data) => {
    return (dispatch) => {
        return apiTemplate
            .post(`/articles`, data)
            .then((res) => {
                dispatch({ type: POST_ARTICLES_DATA, payload: res.data.dbarticles });
            })
            .catch((err) => console.log(err));
    };
};

// Put article
export const putArticle = (id, data) => {
    return (dispatch) => {
        return apiTemplate
            .put(`/articles/${id}`, data)
            .then((res) => {
                dispatch({ type: PUT_ARTICLES_DATA, payload: res.data.dbarticles });
            })
            .catch((err) => console.log(err));
    };
};

// DeleteOne article
export const deleteOneArticle = (id) => {
    return (dispatch) => {
        return apiTemplate
            .delete(`/articles/${id}`)
            .then((res) => {
                dispatch({ type: DELETEONE_ARTICLES_DATA, payload: res.data.dbarticles });
            })
            .catch((err) => console.log(err));
    };
};

// DeleteAll articles
export const deleteAllArticles = () => {
    return (dispatch) => {
        return apiTemplate
            .delete("/articles")
            .then((res) => {
                dispatch({ type: DELETEALL_ARTICLES_DATA, payload: res.data.dbarticles });
            })
            .catch((err) => console.log(err));
    };
};
