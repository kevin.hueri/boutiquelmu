/*
 * Import - Module
 * *************** */
import { apiTemplate } from "../../config/axios";

/*
 * Import types {...}
 * ****************** */
import {
    GET_IMAGESARTICLES_DATA,
    POST_IMAGESARTICLE_DATA,
    DELETEONE_IMAGEARTICLE_DATA,
    DELETEALL_IMAGESARTICLES_DATA,
    DELETE_IMAGESPERARTICLES_DATA,
} from "./ActionsTypes.js";

/*
* Actions
* ******* */

// Get images articles
export const getAllImagesArticle = () => {
    return (dispatch) => {
        return apiTemplate
            .get("/images")
            .then((res) => {
                dispatch({ type: GET_IMAGESARTICLES_DATA, payload: res.data.dbImagesArticle });
            })
            .catch((err) => console.log(err));
    };
};

// Post image article
export const postImageArticle = (id, data) => {
    return (dispatch) => {
        return apiTemplate
            .post(`/images/${id}`, data)
            .then((res) => {
                dispatch({ type: POST_IMAGESARTICLE_DATA, payload: res.data.dbImagesArticle });
            })
            .catch((err) => console.log(err));
    };
};

// DeleteOne image article
export const deleteOneImagesArticle = (id) => {
    return (dispatch) => {
        return apiTemplate
            .delete(`/images/${id}`)
            .then((res) => {
                dispatch({ type: DELETEONE_IMAGEARTICLE_DATA, payload: res.data.dbImagesArticle });
            })
            .catch((err) => console.log(err));
    };
};

// DeleteAll images articles
export const deleteAllImagesArticles = () => {
    return (dispatch) => {
        return apiTemplate
            .delete("/images")
            .then((res) => {
                dispatch({ type: DELETEALL_IMAGESARTICLES_DATA, payload: res.data.dbImagesArticle });
            })
            .catch((err) => console.log(err));
    };
};

// DeleteAll images articleID
export const deleteAllImagesArticle = (id) => {
    return (dispatch) => {
        return apiTemplate
            .delete(`/images-per-article/${id}`)
            .then((res) => {
                dispatch({ type: DELETE_IMAGESPERARTICLES_DATA, payload: res.data.dbImagesArticle });
            })
            .catch((err) => console.log(err));
    };
};