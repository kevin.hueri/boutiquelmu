/*
 * Import - Module
 * *************** */
import { apiTemplate } from "../../config/axios";

/*
 * Import types {...}
 * ****************** */
import {
    GET_IMAGESANDARTICLES_DATA,
    GET_IMAGESANDARTICLE_DATA,
    GET_ARTICLESPERCOMMANDE_DATA,
    GET_ARTICLESPERCOMMANDES_DATA
} from "./ActionsTypes.js";

/*
* Actions
* ******* */

export const getAllImagesAndArticles = () => {
    return (dispatch) => {
        return apiTemplate
            .get("/articles-images")
            .then((res) => {
                dispatch({ type: GET_IMAGESANDARTICLES_DATA, payload: res.data.db });
            })
            .catch((err) => console.log(err));
    };
};

export const getImageAndArticle = (id) => {
    return (dispatch) => {
        return apiTemplate
            .get(`/articles-images/${id}`)
            .then((res) => {
                dispatch({ type: GET_IMAGESANDARTICLE_DATA, payload: res.data.db });
            })
            .catch((err) => console.log(err));
    };
};

export const getArticlesPerCommande = (id) => {
    return (dispatch) => {
        return apiTemplate
            .get(`/articles-per-commande/${id}`)
            .then((res) => {
                dispatch({ type: GET_ARTICLESPERCOMMANDE_DATA, payload: res.data.db });
            })
            .catch((err) => console.log(err));
    };
};

export const getArticlesPerCommandes = () => {
    return (dispatch) => {
        return apiTemplate
            .get(`/articles-per-commande`)
            .then((res) => {
                dispatch({ type: GET_ARTICLESPERCOMMANDES_DATA, payload: res.data.db });
            })
            .catch((err) => console.log(err));
    };
};