import React, { useState, useEffect } from 'react'
import AdminLayout from '../layout/AdminLayout';
import Table from '@mui/material/Table';
import TableContainer from '@mui/material/TableContainer';
import Paper from '@mui/material/Paper';
import TableBody from '@mui/material/TableBody';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import TableCell from '@mui/material/TableCell';
import Typography from '@mui/material/Typography';
import Button from '@mui/material/Button';
import Box from '@mui/material/Box';
import AddIcon from '@mui/icons-material/Add';
import ListItemIcon from "@mui/material/ListItemIcon";

import PropTypes from 'prop-types';

import { NumericFormat } from 'react-number-format';

import AddArticleCollapse from '../components/boutique/AddArticleCollapse';
import ArticleRow from '../components/boutique/ArticleRow';

import { getAllImagesAndArticles } from '../store/actions/RelationsActions';
import { useDispatch, useSelector } from "react-redux";

const NumericFormatCustom = React.forwardRef(function NumericFormatCustom(props, ref) {
    const { onChange, ...other } = props;
    return (
        <NumericFormat
            {...other}
            getInputRef={ref}
            onValueChange={(values) => { onChange({ target: { name: props.name, value: values.value } }); }}
            valueIsNumericString
        />
    );
});

NumericFormatCustom.propTypes = { name: PropTypes.string.isRequired, onChange: PropTypes.func.isRequired };

export default function Home() {
    const [openAddArticle, setOpenAddArticle] = useState(false);
    const dispatch = useDispatch();

    const handleClickOpenAddArticle = () => { setOpenAddArticle(!openAddArticle) };
    const handleCloseAddArticle = () => { setOpenAddArticle(false) }
    const articlesAndImages = useSelector((state) => state.relations.dataRelations);

    useEffect(() => { dispatch(getAllImagesAndArticles()); }, [dispatch, openAddArticle]);

    return (
        <AdminLayout>
            <Box sx={{ display: 'flex', justifyContent: 'space-between', mb: '50px' }}>
                <Typography variant='h1' sx={{ fontSize: "2rem" }}>
                    Gestion du stock
                </Typography>
                <Button
                    sx={{ color: '#fff', textTransform: 'inherit', display: 'block' }}
                    variant="contained"
                    onClick={() => handleClickOpenAddArticle()}
                >
                    <ListItemIcon sx={{ verticalAlign: "top", minWidth: "35px" }}>
                        <AddIcon sx={{ color: "#fff" }} />
                    </ListItemIcon>
                    Ajouter
                </Button>
            </Box>
            <TableContainer component={Paper} sx={{ boxShadow: 'none' }}>
                <Table>
                    <TableHead>
                        <TableRow>
                            <TableCell sx={{ width: '150px', fontWeight: 'bold' }}>Image</TableCell>
                            <TableCell sx={{ width: '400px', fontWeight: 'bold' }}>Nom</TableCell>
                            <TableCell align='center' sx={{ width: '150px', fontWeight: 'bold' }}>Seuil critique</TableCell>
                            <TableCell align='center' sx={{ width: '150px', fontWeight: 'bold' }}>Quantité</TableCell>
                            <TableCell align='center' sx={{ width: '150px', fontWeight: 'bold' }}>Prix</TableCell>
                            <TableCell align='center' sx={{ width: '100px', fontWeight: 'bold' }}>Actions</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {articlesAndImages.map((value, index) => (
                            <ArticleRow
                                key={index}
                                value={value}
                                dispatch={dispatch}
                                NumericFormatCustom={NumericFormatCustom}
                            />
                        ))}
                    </TableBody>
                </Table>
            </TableContainer>
            <AddArticleCollapse
                openAddArticle={openAddArticle}
                onClose={() => handleCloseAddArticle()}
                NumericFormatCustom={NumericFormatCustom}
                dispatch={dispatch}
            />
        </AdminLayout>
    )
}