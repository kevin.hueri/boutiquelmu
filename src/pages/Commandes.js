import React, { useState } from 'react'
import AdminLayout from '../layout/AdminLayout';
import Typography from '@mui/material/Typography';
import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import AddIcon from '@mui/icons-material/Add';
import ListItemIcon from "@mui/material/ListItemIcon";

import NewCommande from '../components/commande/NewCommande';
import ArchiveCommande from '../components/commande/ArchiveCommande';

import { useDispatch } from "react-redux";

function Commande() {
    const dispatch = useDispatch();
    const [openAddCommande, setOpenAddCommande] = useState(false);

    const handleClickOpenAddCommande = () => { setOpenAddCommande(!openAddCommande); };

    return (
        <AdminLayout>
            <Box sx={{ display: 'flex', justifyContent: 'space-between', mb: '50px' }}>
                <Typography variant='h1' sx={{ fontSize: "2rem" }}>
                    Gestion des commandes
                </Typography>
                <Button
                    sx={{ color: '#fff', textTransform: 'inherit', display: 'block' }}
                    variant="contained"
                    onClick={() => handleClickOpenAddCommande()}
                >
                    <ListItemIcon sx={{ verticalAlign: "top", minWidth: "35px" }}>
                        <AddIcon sx={{ color: "#fff" }} />
                    </ListItemIcon>
                    Ajouter
                </Button>
            </Box>
            <Box>
                <NewCommande dispatch={dispatch} openAddCommande={openAddCommande} setOpenAddCommande={setOpenAddCommande} />
                <ArchiveCommande dispatch={dispatch} />
            </Box>
        </AdminLayout >
    )
}

export default Commande
