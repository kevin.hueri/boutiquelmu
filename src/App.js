import { HashRouter, Route, Routes } from "react-router-dom";
import Home from "./pages/Home";
import Commande from "./pages/Commandes";

function App() {
  return (
    <HashRouter>
      <Routes>
        <Route path="/" exact element={< Home />} />
        <Route path="/commande" exact element={< Commande />} />
      </Routes>
    </HashRouter>
  );
}

export default App;
