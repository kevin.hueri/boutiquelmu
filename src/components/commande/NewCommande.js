import React, { useState, useEffect } from 'react'
import TextField from '@mui/material/TextField';
import FormControl from '@mui/material/FormControl';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import Button from '@mui/material/Button';
import Alert from '@mui/material/Alert';
import Snackbar from '@mui/material/Snackbar';
import Collapse from '@mui/material/Collapse';

import { DatePicker } from '@mui/x-date-pickers/DatePicker';

import { postCommandeEvent } from "../../store/actions/CommandeEventActions";
import { getArticlesPerCommandes } from '../../store/actions/RelationsActions';

function ButtonField(props) {
    const {
        setOpenDateButton,
        label,
        id,
        disabled,
        InputProps: { ref } = {},
        inputProps: { 'aria-label': ariaLabel } = {},
    } = props;

    return (
        <Button
            id={id}
            disabled={disabled}
            ref={ref}
            aria-label={ariaLabel}
            onClick={() => setOpenDateButton?.((prev) => !prev)}
            sx={{ color: 'rgba(0, 0, 0, 0.6)', border: "1px solid rgba(0, 0, 0, 0.3)" }}
        >
            {label ?? 'Pick a date'}
        </Button>
    );
}

function ButtonDatePicker(props) {
    const [openDateButton, setOpenDateButton] = useState(false);

    return (
        <DatePicker
            slots={{ field: ButtonField, ...props.slots }}
            slotProps={{ field: { setOpenDateButton } }}
            {...props}
            open={openDateButton}
            onClose={() => setOpenDateButton(false)}
            onOpen={() => setOpenDateButton(true)}
        />
    );
}

function NewCommande(props) {
    const { openAddCommande, setOpenAddCommande, dispatch } = props;
    const [eventName, setEventName] = useState('');
    const [valueDate, setValueDate] = useState(null);
    const [openErreurNewEvent, setOpenErreurNewEvent] = useState(false);
    const [erreurMessage, setErreurMessage] = useState('');
    const [openSuccess, setOpenSuccess] = useState(false);

    const handleSubmitAddEvent = async (e) => {
        if (eventName.length > '' && valueDate !== null) {
            var DateSelect = valueDate.format('YYYY-MM-DD');
            const data = { event: eventName, date: DateSelect }
            await dispatch(postCommandeEvent(data));
            setOpenSuccess(true);
            setOpenAddCommande(false);
        } else {
            if (eventName.length > '' && valueDate === null) {
                setErreurMessage("Date manquante!");
                setOpenErreurNewEvent(true);
            } else if (eventName.length <= '' && valueDate !== null) {
                setErreurMessage("Titre de l'événement manquant!");
                setOpenErreurNewEvent(true);
            } else {
                setErreurMessage("Les champs sont vides!");
                setOpenErreurNewEvent(true);
            }
        }
    };

    const handleClose = () => {
        setOpenSuccess(false);
        setOpenErreurNewEvent(false);
    }

    useEffect(() => { dispatch(getArticlesPerCommandes()); }, [dispatch, openSuccess]);

    return (
        <Collapse in={openAddCommande}>
            <Typography variant='h2' mb={"10px"} sx={{ fontSize: "1.3rem" }}>
                Nouvelle commande
            </Typography>
            <FormControl sx={{ pt: 0, minWidth: 400, maxWidth: 1400, width: '100%', display: 'block' }}>
                <Box sx={{ display: "flex", justifyContent: "space-between" }}>
                    <TextField
                        label="Nom de l'événement"
                        fullWidth
                        sx={{ width: "70%", display: "block" }}
                        onChange={(e) => setEventName(e.target.value)}
                    />
                    <ButtonDatePicker
                        label={`Date: ${valueDate == null ? 'JJ/MM/AAAA' : valueDate.format('DD/MM/YYYY')}`}
                        valueDate={valueDate}
                        onChange={(newValue) => setValueDate(newValue)}
                    />
                    <Button variant="contained" onClick={() => handleSubmitAddEvent()} >
                        Valider
                    </Button>
                </Box>
            </FormControl>

            <Snackbar open={openErreurNewEvent} autoHideDuration={3000} onClose={handleClose} anchorOrigin={{ vertical: 'bottom', horizontal: 'right' }}>
                <Alert severity="error" sx={{ width: '100%' }}>{erreurMessage}</Alert>
            </Snackbar>

            <Snackbar open={openSuccess} autoHideDuration={3000} onClose={handleClose} anchorOrigin={{ vertical: 'bottom', horizontal: 'right' }}>
                <Alert severity="success" sx={{ width: '100%' }}>Nouvel événement créé!</Alert>
            </Snackbar>
        </Collapse>
    )
}

export default NewCommande