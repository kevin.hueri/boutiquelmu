import React, { useState, useEffect } from 'react';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogActions from '@mui/material/DialogActions';
import DialogTitle from '@mui/material/DialogTitle';
import Button from '@mui/material/Button';
import Dialog from '@mui/material/Dialog';
import DeleteIcon from '@mui/icons-material/Delete';
import ListItem from '@mui/material/ListItem';
import ListItemText from '@mui/material/ListItemText';
import TextField from '@mui/material/TextField';
import EditIcon from '@mui/icons-material/Edit';
import DoneIcon from '@mui/icons-material/Done';
import Alert from '@mui/material/Alert';
import Snackbar from '@mui/material/Snackbar';

import PropTypes from 'prop-types';

import { deleteOneArticleCommande, putArticleCommande } from "../../store/actions/ArticlesCommandeActions";
import { getArticlesPerCommandes } from '../../store/actions/RelationsActions';
import { getAllArticles, putArticle } from '../../store/actions/ArticlesActions';

import { NumericFormat } from 'react-number-format';

const NumericFormatCustom = React.forwardRef(function NumericFormatCustom(props, ref) {
    const { onChange, ...other } = props;
    return (
        <NumericFormat
            {...other}
            getInputRef={ref}
            onValueChange={(values) => { onChange({ target: { name: props.name, value: values.value } }); }}
            valueIsNumericString
        />
    );
});

NumericFormatCustom.propTypes = { name: PropTypes.string.isRequired, onChange: PropTypes.func.isRequired };

function ItemList(props) {
    const { labelId, val, dataArticle, dispatch } = props
    const [openDeleteArticleCommande, setOpenDeleteArticleCommande] = useState(false);
    const [quantite, setQuantite] = useState();
    const [modif, setModif] = useState(false);
    const [openSeuilCritique, setOpenSeuilCritique] = useState(false);
    const [openErreur, setOpenErreur] = useState(false);
    const [erreurMessage, setErreurMessage] = useState('');

    var quantiteFixe = Number(val.articlesCommande.quantite);

    const handleDeleteDialog = async (event) => {
        if (quantite) {
            var nouvelleQuantite = Number(val.nombre) + Number(quantite);
        } else {
            var nouvelleQuantite = Number(val.nombre) + Number(val.articlesCommande.quantite);
        }
        console.log(quantite)
        const data2 = { nombre: Number(nouvelleQuantite) };
        await dispatch(deleteOneArticleCommande(event.articlesCommande.id));
        await dispatch(putArticle(val.id, data2));
        setOpenDeleteArticleCommande(false);
    }

    const handleDeleteArticle = () => { setOpenDeleteArticleCommande(true); };
    const handleUpdateQuantiteArticle = () => {
        setModif(!modif);
    };
    const handleValidateUpdateQuantiteArticle = async (e) => {
        var nouvelleQuantite = Number(val.nombre) + Number(quantiteFixe) - Number(quantite);
        if (Number(nouvelleQuantite) >= 0) {
            if (Number(nouvelleQuantite) > Number(val.seuilCritique)) {
                const data = { quantite: Number(quantite) };
                const data2 = { nombre: Number(nouvelleQuantite) };
                await dispatch(putArticleCommande(val.articlesCommande.id, data));
                await dispatch(putArticle(val.id, data2));
                setModif(false);
            } else {
                setOpenSeuilCritique(true);
            }
        } else {
            setErreurMessage("La quantité doit être inférieur aux nombre d\'article restant");
            setOpenErreur(true);
        }
    }
    const handleUpdateQuantiteDialog = async (e) => {
        var nouvelleQuantite = Number(val.nombre) + Number(quantiteFixe) - Number(quantite);
        const data = { quantite: Number(quantite) };
        const data2 = { nombre: Number(nouvelleQuantite) };
        await dispatch(putArticleCommande(val.articlesCommande.id, data));
        await dispatch(putArticle(val.id, data2));
        setModif(false);
        setOpenSeuilCritique(false);
    }
    const handleClose = () => { setOpenErreur(false); };
    const handleCloseDeleteDialog = () => { setOpenDeleteArticleCommande(false); };
    const handleCloseUpdateQuantiteDialog = () => { setOpenSeuilCritique(false); };
    useEffect(() => {
        setQuantite(val.articlesCommande.quantite);
        dispatch(getAllArticles());
        dispatch(getArticlesPerCommandes());
    }, [dispatch, openDeleteArticleCommande, modif, dataArticle]);

    return (
        <ListItem role="listitem" sx={{ my: 1 }}>
            <Button onClick={handleDeleteArticle} color="error">
                <DeleteIcon />
            </Button>
            <ListItemText id={labelId} primary={val.nom + ' (restant: ' + val.nombre + ')'} />
            {modif === false &&
                <ListItemText id={labelId} primary={val.articlesCommande.quantite} sx={{ textAlign: 'end' }} />
            }
            {modif === true &&
                <TextField
                    variant="standard"
                    defaultValue={val.articlesCommande.quantite}
                    sx={{ width: "100px", display: 'block' }}
                    inputProps={{ sx: { textAlign: 'end' } }}
                    onChange={(e) => setQuantite(e.target.value)}
                    name="numberformat"
                    InputProps={{ inputComponent: NumericFormatCustom }}
                />
            }
            {modif === false &&
                <Button onClick={handleUpdateQuantiteArticle}>
                    <EditIcon />
                </Button>
            }
            {modif === true &&
                <Button onClick={() => handleValidateUpdateQuantiteArticle()} color="success">
                    <DoneIcon />
                </Button>
            }
            <Dialog
                open={openDeleteArticleCommande}
                onClose={handleCloseDeleteDialog}
                aria-labelledby="scroll-dialog-title"
                aria-describedby="scroll-dialog-description"
            >
                <DialogTitle id="scroll-dialog-title">Suppression de {val.nom}</DialogTitle>
                <DialogContent>
                    <DialogContentText id="scroll-dialog-description" tabIndex={-1} >
                        Voulez-vous vraiment supprimer l'article {val.nom}
                    </DialogContentText>
                </DialogContent>
                <DialogActions>
                    <Button onClick={(e) => handleDeleteDialog(val)}>Oui</Button>
                    <Button onClick={handleCloseDeleteDialog}>Non</Button>
                </DialogActions>
            </Dialog>
            <Dialog
                open={openSeuilCritique}
                onClose={handleCloseUpdateQuantiteDialog}
                aria-labelledby="scroll-dialog-title"
                aria-describedby="scroll-dialog-description"
            >
                <DialogTitle id="scroll-dialog-title">Seuil critique de {val.nom}</DialogTitle>
                <DialogContent>
                    <DialogContentText id="scroll-dialog-description" tabIndex={-1} >
                        Vous allez passer sous le seuil critique ({val.seuilCritique}). Voulez-vous continuer?
                    </DialogContentText>
                </DialogContent>
                <DialogActions>
                    <Button
                        onClick={(e) => handleUpdateQuantiteDialog(val)}
                    >Oui</Button>
                    <Button onClick={handleCloseUpdateQuantiteDialog}>Non</Button>
                </DialogActions>
            </Dialog>
            <Snackbar open={openErreur} autoHideDuration={3000} onClose={handleClose} anchorOrigin={{ vertical: 'bottom', horizontal: 'right' }}>
                <Alert severity="error" sx={{ width: '100%' }}>{erreurMessage}</Alert>
            </Snackbar>
        </ListItem>

    )
}

export default ItemList;