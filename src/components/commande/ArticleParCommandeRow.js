import React, { useEffect } from 'react'
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import CardMedia from '@mui/material/CardMedia';

import { urlImg } from '../../config/url';

import { getAllImagesAndArticles } from '../../store/actions/RelationsActions';
import { useSelector } from "react-redux";

function ArticleParCommandeRow(props) {
    const { valueArticle, dispatch } = props;

    const articlesAndImages = useSelector((state) => state.relations.dataRelations);
    useEffect(() => { dispatch(getAllImagesAndArticles()); }, [dispatch]);

    return (
        <React.Fragment>
            <Box>
                {(valueArticle.articlesCommande.quantite) <= 1 &&
                    <Box sx={{ display: 'flex', alignItems: 'flex-end' }}>
                        {articlesAndImages.map((value, index) => (
                            <Box key={index}>
                                {(value.id) === (valueArticle.id) &&
                                    <Box>
                                        {(value.imagesArticles).length > '' &&
                                            <CardMedia
                                                component="img"
                                                image={`${urlImg + value.imagesArticles[0].image}`}
                                                alt={value.imagesArticles[0].name}
                                                sx={{ width: '75px', margin: 'auto' }}
                                            />
                                        }
                                        {(value.imagesArticles).length <= '' &&
                                            <CardMedia
                                                component="img"
                                                image={`${urlImg}/assets/images/noImage.jpg`}
                                                alt="No_photography"
                                                sx={{ width: '75px', margin: 'auto' }}
                                            />
                                        }
                                    </Box>
                                }
                            </Box>
                        ))}
                        <Typography sx={{ pb: '10px' }}>
                            {valueArticle.nom}: <b>{valueArticle.articlesCommande.quantite} pièce</b>
                        </Typography>
                    </Box>
                }
                {(valueArticle.articlesCommande.quantite) > 1 &&
                    <Box sx={{ display: 'flex', alignItems: 'flex-end' }}>
                        {articlesAndImages.map((value, index) => (
                            <Box key={index}>
                                {(value.id) === (valueArticle.id) &&
                                    <Box>
                                        {(value.imagesArticles).length > '' &&
                                            <CardMedia
                                                component="img"
                                                image={`${urlImg + value.imagesArticles[0].image}`}
                                                alt={value.imagesArticles[0].name}
                                                sx={{ width: '75px', margin: 'auto' }}
                                            />
                                        }
                                        {(value.imagesArticles).length <= '' &&
                                            <CardMedia
                                                component="img"
                                                image={`${urlImg}/assets/images/noImage.jpg`}
                                                alt="No_photography"
                                                sx={{ width: '75px', margin: 'auto' }}
                                            />
                                        }
                                    </Box>
                                }
                            </Box>
                        ))}
                        <Typography sx={{ pb: '10px' }}>
                            {valueArticle.nom}: <b>{valueArticle.articlesCommande.quantite} pièces</b>
                        </Typography>
                    </Box>
                }
            </Box>
        </React.Fragment>
    )
}

export default ArticleParCommandeRow
