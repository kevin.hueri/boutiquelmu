import React, { useState, useEffect } from 'react'
import Box from '@mui/material/Box';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import Snackbar from '@mui/material/Snackbar';
import Alert from '@mui/material/Alert';

import EventRaw from './EventRaw';

import { getAllArticles } from '../../store/actions/ArticlesActions';
import { getAllImagesAndArticles } from '../../store/actions/RelationsActions';
import { getArticlesPerCommandes } from '../../store/actions/RelationsActions';
import { useSelector } from "react-redux";

export default function ArchiveCommande(props) {
    const { dispatch } = props;
    const [openSuccess, setOpenSuccess] = useState(false);

    const commandeArticles = useSelector((state) => state.relations.dataRelationsCommandeArticles);

    const handleCloseAlert = () => { setOpenSuccess(false); }

    useEffect(() => {
        dispatch(getArticlesPerCommandes());
        dispatch(getAllImagesAndArticles());
        dispatch(getAllArticles());
    }, [dispatch]);

    return (
        <Box>
            <TableContainer component={Paper} sx={{ boxShadow: 'none' }}>
                <Table aria-label="collapsible table">
                    <TableHead>
                        <TableRow>
                            <TableCell sx={{ width: '50px' }} />
                            <TableCell sx={{ fontWeight: 'bold' }}>Evenement</TableCell>
                            <TableCell sx={{ width: '150px', fontWeight: 'bold' }}>Date</TableCell>
                            <TableCell sx={{ width: '50px', fontWeight: 'bold' }} >Supprimer</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {commandeArticles.map((value, index) => (
                            <EventRaw key={index} value={value} setOpenSuccess={setOpenSuccess} dispatch={dispatch} />
                        ))
                        }
                    </TableBody>
                </Table>
            </TableContainer>
            <Snackbar open={openSuccess} autoHideDuration={3000} onClose={handleCloseAlert} anchorOrigin={{ vertical: 'bottom', horizontal: 'right' }}>
                <Alert severity="success" sx={{ width: '100%' }}>Evenement supprimé!</Alert>
            </Snackbar>
        </Box >
    )
}