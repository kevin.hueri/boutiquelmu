import React, { useState } from 'react';
import IconButton from '@mui/material/IconButton';
import TableCell from '@mui/material/TableCell';
import TableRow from '@mui/material/TableRow';
import KeyboardArrowDownIcon from '@mui/icons-material/KeyboardArrowDown';
import KeyboardArrowUpIcon from '@mui/icons-material/KeyboardArrowUp';
import Collapse from '@mui/material/Collapse';
import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import DeleteIcon from '@mui/icons-material/Delete';
import Tooltip from '@mui/material/Tooltip';

import DeleteEvent from './DeleteEvent';
import EditCommande from './EditCommande';
import ArticleParCommandeRow from './ArticleParCommandeRow';

import { useSelector } from "react-redux";

function EventRaw(props) {
    const { value, setOpenSuccess, dispatch } = props;
    const [open, setOpen] = useState(false);
    const [openDialogDeleteEvent, setOpenDialogDeleteEvent] = useState(false);
    const [openDialogUpdateArticlesEvent, setOpenDialogUpdateArticlesEvent] = useState(false);

    var Days = (value.date).substr(8, 2);
    var Months = (value.date).substr(5, 2);
    var Years = (value.date).substr(0, 4);
    var Date = Days + "/" + Months + "/" + Years;

    const articles = useSelector((state) => state.article.dataArticles);

    const handleOpenDeleteEvent = () => { setOpenDialogDeleteEvent(true); };
    const handleClose = () => { setOpenDialogDeleteEvent(false); };
    const handleCloseUpdateArticleEvent = () => { setOpenDialogUpdateArticlesEvent(false); }
    const handleClickOpenAddArticle = () => { setOpenDialogUpdateArticlesEvent(true); }

    return (
        <React.Fragment>
            <TableRow sx={{ '& > *': { borderBottom: 'unset' } }}>
                <TableCell sx={{ borderBottom: "none" }}>
                    <IconButton aria-label="expand row" size="small" onClick={() => setOpen(!open)}>
                        {open ? <KeyboardArrowUpIcon /> : <KeyboardArrowDownIcon />}
                    </IconButton>
                </TableCell>
                <TableCell align="left" component="th" scope="row" sx={{ borderBottom: "none" }}>{value.event}</TableCell>
                <TableCell sx={{ borderBottom: "none" }}>{Date}</TableCell>
                <TableCell sx={{ borderBottom: "none" }}>
                    <Tooltip title="Supprimer" onClick={() => handleOpenDeleteEvent()}>
                        <IconButton>
                            <DeleteIcon />
                        </IconButton>
                    </Tooltip>
                </TableCell>
            </TableRow>
            <TableRow>
                <TableCell style={{ paddingBottom: 0, paddingTop: 0 }} colSpan={6}>
                    <Collapse in={open} timeout="auto" unmountOnExit>
                        <Box>
                            <Box sx={{ mt: "10px", pl: '100px' }}>
                                {(value.articles).length > '' && (value.articles).map((valueArticle, index) => (
                                    <ArticleParCommandeRow
                                        key={index}
                                        valueArticle={valueArticle}
                                        dispatch={dispatch}
                                    />
                                ))}
                            </Box>
                            <Button
                                sx={{ my: 2, textTransform: 'inherit', width: '200px', mx: 'auto', display: 'block' }}
                                variant="contained"
                                size="medium"
                                fullWidth
                                onClick={() => handleClickOpenAddArticle()}
                            >
                                Modifier la commande
                            </Button>
                        </Box>
                    </Collapse>
                </TableCell>
                <DeleteEvent
                    value={value}
                    openDialogDeleteEvent={openDialogDeleteEvent}
                    setOpenDialogDeleteEvent={setOpenDialogDeleteEvent}
                    setOpenSuccess={setOpenSuccess}
                    handleClose={handleClose}
                    dispatch={dispatch}
                />
                <EditCommande
                    value={value}
                    openDialogUpdateArticlesEvent={openDialogUpdateArticlesEvent}
                    handleCloseUpdateArticleEvent={handleCloseUpdateArticleEvent}
                    articles={articles}
                    dispatch={dispatch}
                />
            </TableRow>
        </React.Fragment>
    )
}

export default EventRaw
