import React, { useState, useEffect } from 'react'
import Dialog from '@mui/material/Dialog';
import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import ArrowBackIcon from '@mui/icons-material/ArrowBack';
import DialogTitle from '@mui/material/DialogTitle';
import Grid from '@mui/material/Grid';
import Paper from '@mui/material/Paper';
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import ListItemText from '@mui/material/ListItemText';
import Typography from '@mui/material/Typography';
import AddIcon from '@mui/icons-material/Add';
import ListItemIcon from '@mui/material/ListItemIcon';
import Snackbar from '@mui/material/Snackbar';
import Alert from '@mui/material/Alert';

import ItemList from './ItemList';

import { postArticleCommande } from "../../store/actions/ArticlesCommandeActions";
import { getArticlesPerCommandes } from '../../store/actions/RelationsActions';

function EditCommande(props) {
    const { value, openDialogUpdateArticlesEvent, handleCloseUpdateArticleEvent, articles, dispatch } = props;
    const [dataArticle, setDataArticle] = useState();
    const [erreurMessage, setErreurMessage] = useState('');
    const [openErreurNewEvent, setOpenErreurNewEvent] = useState(false);

    const handleToggle = async (item) => {
        const include = (value.articles).filter(function (val) {
            return val.id == item.id;
        })
        if (include.length > '') {
            setErreurMessage("Article déjà présent!");
            setOpenErreurNewEvent(true);
        } else {
            const data = { articleId: item.id, quantite: 0 }
            await dispatch(postArticleCommande(value.id, data));
            setDataArticle(data);
        }
    }
    const handleClose = () => { setOpenErreurNewEvent(false); }

    useEffect(() => {
        dispatch(getArticlesPerCommandes());
    }, [dispatch, dataArticle, openErreurNewEvent]);

    return (
        <Dialog
            open={openDialogUpdateArticlesEvent}
            onClose={handleCloseUpdateArticleEvent}
            fullScreen
            aria-labelledby="scroll-dialog-title"
            aria-describedby="scroll-dialog-description"
            sx={{ left: "280px", zIndex: 1000 }}>
            <Box sx={{ height: '50px', mt: "20px", mr: '30px', display: 'flex', justifyContent: 'space-between' }}>
                <Button
                    sx={{ height: '50px', color: 'black', fontSize: '17px', textTransform: 'inherit' }}
                    onClick={handleCloseUpdateArticleEvent}
                >
                    <ArrowBackIcon sx={{ color: 'black', fontSize: '30px', mr: '5px' }} />
                    Gestion des commandes
                </Button>
            </Box>
            <DialogTitle id="scroll-dialog-title" sx={{ p: "0 24px 25px", fontSize: '1.2rem', color: "black" }} >
                Mise à jour de l'article <strong>{value.event}</strong>
            </DialogTitle>
            <Grid container spacing={2} ml={5} alignItems="center">
                <Grid item>
                    <Paper sx={{ width: { xs: 250, md: 300, lg: 400 }, height: 500, overflow: 'auto' }}>
                        <Typography sx={{ textAlign: 'center', fontWeight: 'bold', my: 2 }} >
                            Commande en cours
                        </Typography>
                        <List dense component="div" role="list">
                            {(value.articles).map((val, index) => {
                                const labelId = `transfer-list-item-${val.id}-label`;
                                return (<ItemList key={index} labelId={labelId} val={val} dispatch={dispatch} dataArticle={dataArticle} />);
                            })}
                        </List>
                    </Paper>
                </Grid>
                <Grid item>
                    <Paper sx={{ width: { xs: 200, md: 250, lg: 300 }, height: 500, overflow: 'auto' }}>
                        <Typography sx={{ textAlign: 'center', fontWeight: 'bold', my: 2 }} >
                            Articles disponibles
                        </Typography>
                        <List dense component="div" role="list">
                            {articles && articles.map((item, index) => {
                                const labelId = `transfer-list-item-${item.id}-label`;
                                return (
                                    <ListItem key={index} role="listitem" button onClick={(e) => handleToggle(item)} sx={{ py: 1 }} >
                                        <ListItemIcon >
                                            <AddIcon />
                                        </ListItemIcon>
                                        <ListItemText id={labelId} primary={item.nom} />
                                    </ListItem>
                                );
                            })}
                        </List>
                    </Paper>
                </Grid>
                <Snackbar open={openErreurNewEvent} autoHideDuration={3000} onClose={handleClose} anchorOrigin={{ vertical: 'bottom', horizontal: 'right' }}>
                    <Alert severity="error" sx={{ width: '100%' }}>{erreurMessage}</Alert>
                </Snackbar>
            </Grid>
        </Dialog>
    )
}

export default EditCommande