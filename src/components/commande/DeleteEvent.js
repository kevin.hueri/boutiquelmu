import React, { useEffect } from 'react'
import Dialog from '@mui/material/Dialog';
import DialogTitle from '@mui/material/DialogTitle';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogActions from '@mui/material/DialogActions';
import Button from '@mui/material/Button';

import { deleteOneCommandeEvent } from "../../store/actions/CommandeEventActions";
import { getArticlesPerCommandes } from '../../store/actions/RelationsActions';
import { putArticle } from '../../store/actions/ArticlesActions';

export default function DeleteEvent(props) {
    const { value, openDialogDeleteEvent, setOpenDialogDeleteEvent, setOpenSuccess, handleClose, dispatch } = props;

    const handleSubmitDeleteEvent = async () => {
        (value.articles).forEach((element) => {
            var nouvelleQuantite = Number(element.nombre) + Number(element.articlesCommande.quantite);
            const data2 = { nombre: Number(nouvelleQuantite) };
            dispatch(putArticle(element.id, data2));
        });
        await dispatch(deleteOneCommandeEvent(value.id));
        setOpenDialogDeleteEvent(false);
        setOpenSuccess(true);
    }

    useEffect(() => {
        dispatch(getArticlesPerCommandes())
    }, [dispatch, openDialogDeleteEvent]);

    return (
        <Dialog
            open={openDialogDeleteEvent}
            onClose={handleClose}
            aria-labelledby="scroll-dialog-title"
            aria-describedby="scroll-dialog-description"
        >
            <DialogTitle id="scroll-dialog-title">Suppression de {value.event}</DialogTitle>
            <DialogContent>
                <DialogContentText
                    id="scroll-dialog-description"
                    tabIndex={-1}
                >
                    Voulez-vous vraiment supprimer la commande {value.event}
                </DialogContentText>
            </DialogContent>
            <DialogActions>
                <Button sx={{ color: 'red' }} onClick={() => handleSubmitDeleteEvent()}>Oui</Button>
                <Button onClick={handleClose}>Non</Button>
            </DialogActions>
        </Dialog>
    )
}