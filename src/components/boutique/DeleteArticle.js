import React from 'react'
import Dialog from '@mui/material/Dialog';
import DialogTitle from '@mui/material/DialogTitle';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogActions from '@mui/material/DialogActions';
import Button from '@mui/material/Button';

import { deleteOneArticle } from '../../store/actions/ArticlesActions';
import { deleteAllImagesArticle } from '../../store/actions/ImagesArticleActions';

function DeleteArticle(props) {
    const { value, openDeleteArticleRow, setOpenDeleteArticleRow, setOpenUpdateArticleRow, handleCloseDeleteDialog, dispatch } = props;

    const handleSubmitDeleteArticle = async () => {
        await dispatch(deleteOneArticle(value.id));
        await dispatch(deleteAllImagesArticle(value.id));
        setOpenDeleteArticleRow(false);
        setOpenUpdateArticleRow(false);
    }

    return (
        <Dialog
            open={openDeleteArticleRow}
            onClose={handleCloseDeleteDialog}
            aria-labelledby="scroll-dialog-title"
            aria-describedby="scroll-dialog-description"
        >
            <DialogTitle id="scroll-dialog-title">Suppression de {value.nom}</DialogTitle>
            <DialogContent>
                <DialogContentText id="scroll-dialog-description" tabIndex={-1} >
                    Voulez-vous vraiment supprimer l'article {value.nom}
                </DialogContentText>
            </DialogContent>
            <DialogActions>
                <Button onClick={() => handleSubmitDeleteArticle()}>Oui</Button>
                <Button onClick={handleCloseDeleteDialog}>Non</Button>
            </DialogActions>
        </Dialog>
    )
}

export default DeleteArticle