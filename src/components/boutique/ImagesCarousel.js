import React, { useState, useEffect } from 'react'
import ImageListItem from '@mui/material/ImageListItem';
import DeleteIcon from '@mui/icons-material/Delete';
import ImageListItemBar from '@mui/material/ImageListItemBar';
import IconButton from '@mui/material/IconButton';
import Button from '@mui/material/Button';
import DialogTitle from '@mui/material/DialogTitle';
import Dialog from '@mui/material/Dialog';
import Box from '@mui/material/Box';

import { urlImg } from '../../config/url';

import { deleteOneImagesArticle } from '../../store/actions/ImagesArticleActions';
import { getAllImagesAndArticles } from '../../store/actions/RelationsActions';

export default function ImagesCarousel(props) {
    const { step, dispatch } = props;
    const [openDeleteImageDialog, setOpenDeleteImageDialog] = useState(false);

    const handleDeleteImageDialog = () => {
        setOpenDeleteImageDialog(!openDeleteImageDialog)
    }

    const handleClickDeleteImage = () => {
        dispatch(deleteOneImagesArticle(step.id));
        setOpenDeleteImageDialog(false);
    }

    const imageAlt = (step.name).substring(14);

    useEffect(() => {
        dispatch(getAllImagesAndArticles())
    }, [dispatch, openDeleteImageDialog]);

    return (
        <ImageListItem>
            <img
                width="100%"
                src={`${urlImg + step.image}`}
                alt={imageAlt}
            />
            <ImageListItemBar
                sx={{ background: 'linear-gradient(to bottom, rgba(0,0,0,0.7) 0%, rgba(0,0,0,0.3) 70%, rgba(0,0,0,0) 100%)' }}
                title={imageAlt}
                position="top"
                actionIcon={
                    <IconButton
                        aria-label={`star ${step.name}`}
                        onClick={() => handleDeleteImageDialog()}
                    >
                        <DeleteIcon sx={{ color: '#ff0000' }} />
                    </IconButton>
                }
                actionPosition="right"
            />
            <Dialog onClose={() => handleDeleteImageDialog()} open={openDeleteImageDialog}>
                <DialogTitle sx={{ textDecoration: 'none' }}>
                    Voulez vous vraiment supprimer l'image {step.name}?
                </DialogTitle>
                <Box sx={{ display: 'flex', justifyContent: 'space-around', my: 2 }}>
                    <Button
                        variant="contained"
                        color="error"
                        onClick={() => handleClickDeleteImage()}
                        sx={{ py: 2, width: 150 }}
                    >
                        Oui
                    </Button>
                    <Button
                        variant="contained"
                        color="info"
                        onClick={() => handleDeleteImageDialog()}
                        sx={{ py: 2, width: 150 }}
                    >
                        Non
                    </Button>
                </Box>
            </Dialog>
        </ImageListItem>
    )
}