import React, { useState, useEffect } from 'react'
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogTitle from '@mui/material/DialogTitle';
import Box from '@mui/material/Box';
import List from '@mui/material/List';
import TextField from '@mui/material/TextField';
import FormControl from '@mui/material/FormControl';
import Button from '@mui/material/Button';
import Alert from '@mui/material/Alert';
import DeleteIcon from '@mui/icons-material/Delete';
import ArrowBackIcon from '@mui/icons-material/ArrowBack';
import Snackbar from '@mui/material/Snackbar';

import DeleteArticle from './DeleteArticle';
import { urlImg } from '../../config/url';
import ImagesCarousel from './ImagesCarousel';

import { Swiper, SwiperSlide } from "swiper/react";
import { FreeMode, Navigation, Thumbs } from "swiper/modules";

import { putArticle } from '../../store/actions/ArticlesActions';
import { postImageArticle } from "../../store/actions/ImagesArticleActions";
import { getAllImagesAndArticles } from '../../store/actions/RelationsActions';

import "swiper/css";
import "swiper/css/free-mode";
import "swiper/css/navigation";
import "swiper/css/thumbs";

function UpdateArticle(props) {
    const { value, openUpdateArticleRow, setOpenUpdateArticleRow, handleClose, dispatch, NumericFormatCustom } = props;
    const [valuesNom, setValuesNom] = useState(value.nom);
    const [valuesDescription, setValuesDescription] = useState(value.description);
    const [valuesPrix, setValuesPrix] = useState(value.prix);
    const [valuesSeuilCritique, setValuesSeuilCritique] = useState(value.seuilCritique);
    const [image, setImage] = useState({});
    const [thumbsSwiper, setThumbsSwiper] = useState(null);
    const [imageUrl, setImageUrl] = useState(null);
    const [openErreurUpdateArticle, setOpenErreurUpdateArticle] = useState(false);
    const [openSuccess, setOpenSuccess] = useState(false);
    const [openDeleteArticleRow, setOpenDeleteArticleRow] = useState(false);

    const handleSubmitUpdateArticle = async () => {
        if (image.name) {
            const formData = new FormData();
            formData.append('image', image);
            await dispatch(postImageArticle(value.id, formData));
            setImage({});
            setImageUrl(null);
            setOpenSuccess(true);
        }
        if (valuesNom.length > '' && valuesDescription.length > '' && valuesPrix >= 0 && valuesSeuilCritique >= 0) {
            const data = {
                nom: valuesNom,
                prix: Number(valuesPrix),
                nombre: Number(value.nombre),
                description: valuesDescription,
                seuilCritique: Number(valuesSeuilCritique)
            };
            await dispatch(putArticle(value.id, data));
            setOpenSuccess(true);
        } else {
            setOpenErreurUpdateArticle(true);
        }
    }

    const handleCloseAlert = () => {
        setOpenSuccess(false);
        setOpenErreurUpdateArticle(false);
    };

    const handleOpenDeleteArticle = () => { setOpenDeleteArticleRow(true); }
    const handleCloseDeleteDialog = () => { setOpenDeleteArticleRow(false); }

    useEffect(() => { setThumbsSwiper(null) }, [handleClose]);
    useEffect(() => { if (image.name) { setImageUrl(URL.createObjectURL(image)); } }, [image]);
    useEffect(() => {
        setTimeout(() => {
            dispatch(getAllImagesAndArticles())
        }, 1000)
    }, [dispatch, image, valuesSeuilCritique, valuesNom, valuesDescription]);

    useEffect(() => {
        dispatch(getAllImagesAndArticles())
        if (openErreurUpdateArticle === true) {
            setTimeout(() => {
                setOpenErreurUpdateArticle(false)
            }, 3000)
        }
        if (openSuccess === true) {
            setTimeout(() => {
                setOpenSuccess(false)
            }, 3000)
        }
    }, [openErreurUpdateArticle, openSuccess, dispatch]);

    return (
        <React.Fragment>
            <Dialog
                open={openUpdateArticleRow}
                onClose={handleClose}
                fullScreen
                aria-labelledby="scroll-dialog-title"
                aria-describedby="scroll-dialog-description"
                sx={{ left: "280px", zIndex: 1000 }}
            >
                <Box sx={{ height: '50px', mt: "20px", mr: '30px', display: 'flex', justifyContent: 'space-between' }}>
                    <Button
                        sx={{ height: '50px', color: 'black', fontSize: '17px', textTransform: 'inherit' }}
                        onClick={handleClose}
                    >
                        <ArrowBackIcon sx={{ color: 'black', fontSize: '30px', mr: '5px' }} />
                        Gestion du stock
                    </Button>
                    <Button
                        sx={{ height: '50px', color: 'black', fontSize: '17px', textTransform: 'inherit' }}
                        onClick={() => handleOpenDeleteArticle()}
                    >
                        <DeleteIcon sx={{ color: 'red', fontSize: '30px', mr: '5px' }} />
                    </Button>
                </Box>
                <DialogContent sx={{ display: "flex", justifyContent: "space-between" }}>
                    <Box sx={{ width: "49%" }}>
                        {(value.imagesArticles).length > '' &&
                            <Box>
                                <Swiper
                                    style={{
                                        "--swiper-navigation-color": "#fff",
                                        "--swiper-pagination-color": "#fff",
                                    }}
                                    loop={true}
                                    spaceBetween={10}
                                    slidesPerView={1}
                                    navigation={true}
                                    thumbs={{ swiper: thumbsSwiper }}
                                    modules={[FreeMode, Navigation, Thumbs]}
                                    className="mySwiper2"
                                >
                                    {(value.imagesArticles).map((step, index) => (
                                        <SwiperSlide key={index}>
                                            <ImagesCarousel step={step} dispatch={dispatch} />
                                        </SwiperSlide>
                                    ))}
                                </Swiper>
                                <Swiper
                                    onSwiper={setThumbsSwiper}
                                    loop={true}
                                    spaceBetween={10}
                                    slidesPerView={4}
                                    freeMode={true}
                                    watchSlidesProgress={true}
                                    modules={[FreeMode, Navigation, Thumbs]}
                                    className="mySwiper"
                                >
                                    {(value.imagesArticles).map((step, index) => (
                                        <SwiperSlide key={index}>
                                            <img width="100%" src={`${urlImg + step.image}`} alt={step.name} />
                                        </SwiperSlide>
                                    ))}
                                </Swiper>
                            </Box>
                        }
                        {(value.imagesArticles).length <= '' &&
                            <Box>
                                <img width="100%" src={`${urlImg}/assets/images/noImage.jpg`} alt="no-photography" />
                            </Box>
                        }
                    </Box>
                    <Box sx={{ width: "49%", display: 'flex', flexDirection: 'column', justifyContent: "space-between" }} >
                        <Box>
                            <DialogTitle id="scroll-dialog-title" sx={{ textAlign: "center", p: "0 24px 25px", fontSize: '1.2rem', color: "black" }} >
                                Mise à jour de l'article <strong>{value.nom}</strong>
                            </DialogTitle>
                            <List sx={{ pt: 0, mx: 'auto' }}>
                                <FormControl sx={{ pt: 0, mx: 'auto', width: '100%' }}>
                                    <TextField
                                        label='nom'
                                        variant="outlined"
                                        fullWidth
                                        sx={{ my: '5px' }}
                                        defaultValue={valuesNom}
                                        onChange={(e) => setValuesNom(e.target.value)}
                                    />
                                    <TextField
                                        id="outlined-multiline-static"
                                        label="Description"
                                        sx={{ my: '5px', width: '100%' }}
                                        defaultValue={valuesDescription}
                                        onChange={(e) => setValuesDescription(e.target.value)}
                                        multiline
                                        rows={4}
                                    />
                                    <TextField
                                        label="Prix"
                                        variant="outlined"
                                        fullWidth
                                        sx={{ my: '5px' }}
                                        defaultValue={valuesPrix}
                                        onChange={(e) => setValuesPrix(e.target.value)}
                                        name="numberformat"
                                        InputProps={{ inputComponent: NumericFormatCustom }}
                                    />
                                    <TextField
                                        label="Seuil critique"
                                        variant="outlined"
                                        fullWidth
                                        sx={{ my: '5px' }}
                                        defaultValue={valuesSeuilCritique}
                                        onChange={(e) => setValuesSeuilCritique(e.target.value)}
                                        name="numberformat"
                                        InputProps={{ inputComponent: NumericFormatCustom }}
                                    />
                                    <TextField
                                        label="Quantité actuelle"
                                        sx={{ my: '5px' }}
                                        value={value.nombre}
                                        disabled
                                    />
                                    <Box sx={{ display: 'flex', justifyContent: "space-between", my: "20px" }}>
                                        <TextField
                                            size="small"
                                            type="file"
                                            id="select-image"
                                            sx={{ my: '5px', width: '45%', display: 'none' }}
                                            inputProps={{ accept: "image/*" }}
                                            onChange={(e) => setImage(e.target.files[0])}
                                        />
                                        <label htmlFor="select-image" style={{ textAlign: 'left' }}>
                                            <Button variant="contained" sx={{ bgcolor: '#1976d2' }} component="span">
                                                Upload Image
                                            </Button>
                                        </label>
                                        <Box sx={{ my: '5px', textAlign: 'left', width: "45%" }} >
                                            {imageUrl && (
                                                <Box textAlign="center" sx={{ border: "1px solid black", width: "100px", objectFit: "cover" }} >
                                                    <img src={imageUrl} alt={image.name} height="100px" width="100%" />
                                                </Box>
                                            )}
                                        </Box>
                                    </Box>
                                </FormControl>
                            </List>
                            <DialogActions sx={{ display: 'flex', justifyContent: 'center' }}>
                                <Button variant="contained" onClick={() => handleSubmitUpdateArticle()} sx={{ p: 2 }}                                >
                                    Valider
                                </Button>
                                <Button onClick={handleClose} sx={{ color: 'black', p: 2 }} >
                                    Retour
                                </Button>
                            </DialogActions>
                        </Box>
                    </Box>
                </DialogContent>
                <Snackbar open={openErreurUpdateArticle} autoHideDuration={3000} onClose={handleCloseAlert} anchorOrigin={{ vertical: 'bottom', horizontal: 'right' }}>
                    <Alert severity="error" sx={{ width: '100%' }}>
                        Aucun champs ne doit être vide
                    </Alert>
                </Snackbar>
                <Snackbar open={openSuccess} autoHideDuration={3000} onClose={handleCloseAlert} anchorOrigin={{ vertical: 'bottom', horizontal: 'right' }}>
                    <Alert severity="success" sx={{ width: '100%' }}>article modifié!</Alert>
                </Snackbar>
                <DeleteArticle
                    value={value}
                    openDeleteArticleRow={openDeleteArticleRow}
                    setOpenDeleteArticleRow={setOpenDeleteArticleRow}
                    setOpenUpdateArticleRow={setOpenUpdateArticleRow}
                    handleCloseDeleteDialog={handleCloseDeleteDialog}
                    dispatch={dispatch}
                />
            </Dialog>
        </React.Fragment>
    )
}

export default UpdateArticle