import React, { useState } from 'react'
import Collapse from '@mui/material/Collapse';
import Typography from '@mui/material/Typography';
import TextField from '@mui/material/TextField';
import Button from '@mui/material/Button';
import Box from '@mui/material/Box';

import { putArticle } from '../../store/actions/ArticlesActions';

function AddQuantite(props) {
    const { value, openAddQuantite, setOpenAddQuantite, setOpenSuccessAdd, NumericFormatCustom, dispatch } = props;
    const [quantite, setQuantite] = useState(null);

    const handleSubmitAdd = async () => {
        if (quantite !== null && quantite > "") {
            var nouvelleQuantite = Number(value.nombre) + Number(quantite);
            const data = {
                nombre: Number(nouvelleQuantite),
            };
            await dispatch(putArticle(value.id, data));
            setOpenSuccessAdd(true);
            setOpenAddQuantite(false);
        }
    }
    return (
        <Collapse in={openAddQuantite}>
            <Typography variant='h2' mb={"10px"} sx={{ fontSize: "1.3rem", textAlign: 'center', mb: '20px' }}>
                Quantité a ajouter
            </Typography>
            <Box sx={{ mb: '20px', display: 'flex', justifyContent: 'space-around', maxWidth: "600px", mx: 'auto' }}>
                <TextField
                    label="Quantité"
                    variant="outlined"
                    fullWidth
                    sx={{ width: "400px", display: 'block' }}
                    onChange={(e) => setQuantite(e.target.value)}
                    name="numberformat"
                    InputProps={{ inputComponent: NumericFormatCustom }}
                />
                <Button variant="contained" onClick={() => handleSubmitAdd()}                                >
                    Valider
                </Button>
            </Box>
        </Collapse>
    )
}

export default AddQuantite