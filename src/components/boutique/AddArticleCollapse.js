import React, { useState, useEffect } from 'react';
import Dialog from '@mui/material/Dialog';
import List from '@mui/material/List';
import TextField from '@mui/material/TextField';
import Box from '@mui/material/Box';
import FormControl from '@mui/material/FormControl';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';
import Alert from '@mui/material/Alert';
import ArrowBackIcon from '@mui/icons-material/ArrowBack';
import Snackbar from '@mui/material/Snackbar';

import { postArticle } from '../../store/actions/ArticlesActions';
import { getAllImagesAndArticles } from '../../store/actions/RelationsActions';

export default function AddArticleCollapse(props) {
    const { onClose, openAddArticle, NumericFormatCustom, dispatch } = props;
    const [nomAdd, setNomAdd] = useState('');
    const [descriptionAdd, setDescriptionAdd] = useState('');
    const [seuilCritiqueAdd, setSeuilCritiqueAdd] = useState(null);
    const [addPrix, setAddPrix] = useState(null);
    const [addQuantite, setAddQuantite] = useState(null);
    const [openErreurAddArticle, setOpenErreurAddArticle] = useState(false);
    const [openSuccess, setOpenSuccess] = useState(false);
    const [erreurMessage, setErreurMessage] = useState('');

    const handleSubmitAddArticle = async (e) => {
        if (nomAdd.length > "") {
            const data = {
                nom: nomAdd,
                prix: addPrix !== null ? Number(addPrix) : 0,
                nombre: addQuantite !== null ? Number(addQuantite) : 0,
                description: descriptionAdd.length > '' ? descriptionAdd : 'null',
                seuilCritique: seuilCritiqueAdd !== null ? Number(seuilCritiqueAdd) : 0
            };
            await dispatch(postArticle(data));
            setOpenSuccess(true);
            setNomAdd('');
            setAddQuantite(null);
            setSeuilCritiqueAdd(null);
            onClose();
        } else {
            setOpenErreurAddArticle(true);
            setErreurMessage("L'article n'a pas de nom!");
        }
    }

    const handleCloseAlert = () => {
        setOpenSuccess(false);
        setOpenErreurAddArticle(false);
    }

    useEffect(() => {
        dispatch(getAllImagesAndArticles())
    }, [openErreurAddArticle, openSuccess, dispatch]);

    return (
        <React.Fragment>
            <Dialog
                open={openAddArticle}
                onClose={onClose}
                fullScreen
                aria-labelledby="scroll-dialog-title"
                aria-describedby="scroll-dialog-description"
                sx={{ left: "280px", zIndex: 1000 }}
            >
                <Box sx={{ height: '50px', mt: "20px", ml: "64px", padding: 0 }}>
                    <Button
                        sx={{ height: '50px', color: 'black', fontSize: '17px', textTransform: 'inherit' }}
                        onClick={onClose}
                    >
                        <ArrowBackIcon sx={{ color: 'black', fontSize: '30px', mr: '5px' }} />
                        Gestion du stock
                    </Button>
                </Box>
                <Box sx={{ padding: '10px 64px 64px' }}>
                    <Typography variant='h1' sx={{ fontSize: "2rem", mb: '50px' }}                >
                        Ajouter un article:
                    </Typography>
                    <List sx={{ pt: 0, minWidth: 400, maxWidth: 600 }}>
                        <FormControl sx={{ pt: 0, minWidth: 400, maxWidth: 600, mx: 'auto', width: '100%' }}>
                            <TextField
                                label="Nom"
                                sx={{ my: '5px' }}
                                onChange={(e) => setNomAdd(e.target.value)}
                            />
                            <TextField
                                id="outlined-multiline-static"
                                label="Description"
                                sx={{ my: '5px' }}
                                onChange={(e) => setDescriptionAdd(e.target.value)}
                                multiline
                                rows={4}
                            />
                            <TextField
                                label="Prix"
                                name="numberformat"
                                onChange={(e) => setAddPrix(e.target.value)}
                                sx={{ my: '5px' }}
                                InputProps={{ inputComponent: NumericFormatCustom }}
                            />
                            <TextField
                                label="Quantité"
                                name="numberformat"
                                onChange={(e) => setAddQuantite(e.target.value)}
                                sx={{ my: '5px' }}
                                InputProps={{ inputComponent: NumericFormatCustom }}
                            />
                            <TextField
                                label="Seuil critique"
                                name="numberformat"
                                onChange={(e) => setSeuilCritiqueAdd(e.target.value)}
                                sx={{ my: '5px' }}
                                InputProps={{ inputComponent: NumericFormatCustom }}
                            />
                        </FormControl>
                        <Box sx={{ display: 'flex', justifyContent: 'space-around', my: 2 }}>
                            <Button
                                variant="contained"
                                color="info"
                                sx={{ py: 2, width: 100 }}
                                onClick={() => handleSubmitAddArticle()}
                            >
                                Valider
                            </Button>
                            <Button
                                color="info"
                                sx={{ py: 2, width: 100, color: 'black' }}
                                onClick={onClose}
                            >
                                Fermer
                            </Button>
                        </Box>
                    </List>
                </Box>
            </Dialog >
            <Snackbar open={openSuccess} autoHideDuration={3000} onClose={handleCloseAlert} anchorOrigin={{ vertical: 'bottom', horizontal: 'right' }}>
                <Alert severity="success" sx={{ width: '100%' }}>Nouvel article créé!</Alert>
            </Snackbar>
            <Snackbar open={openErreurAddArticle} autoHideDuration={3000} onClose={handleCloseAlert} anchorOrigin={{ vertical: 'bottom', horizontal: 'right' }}>
                <Alert severity="error" sx={{ width: '100%' }}>
                    {erreurMessage}
                </Alert>
            </Snackbar>
        </React.Fragment>
    );
}