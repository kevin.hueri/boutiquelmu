import React, { useState, useEffect } from 'react'
import TableRow from '@mui/material/TableRow';
import TableCell from '@mui/material/TableCell';
import Button from '@mui/material/Button';
import Box from '@mui/material/Box';
import CardMedia from '@mui/material/CardMedia';
import CreateIcon from '@mui/icons-material/Create';
import AddIcon from '@mui/icons-material/Add';
import RemoveIcon from '@mui/icons-material/Remove';
import Snackbar from '@mui/material/Snackbar';
import Alert from '@mui/material/Alert';

import { urlImg } from '../../config/url';
import UpdateArticle from './UpdateArticle';
import AddQuantite from './AddQuantite';
import RemoveQuantite from './RemoveQuantite';
import { getAllImagesAndArticles } from '../../store/actions/RelationsActions';

export default function ArticleRow(props) {
    const { value, NumericFormatCustom, dispatch } = props;
    const [openUpdateArticleRow, setOpenUpdateArticleRow] = useState(false);
    const [openAddQuantite, setOpenAddQuantite] = useState(false);
    const [openRemoveQuantite, setOpenRemoveQuantite] = useState(false);
    const [openSuccessAdd, setOpenSuccessAdd] = useState(false);
    const [openSuccessRemove, setOpenSuccessRemove] = useState(false);

    const handleClose = () => { setOpenUpdateArticleRow(false); }
    const handleCloseAlert = () => {
        setOpenSuccessAdd(false);
        setOpenSuccessRemove(false);
    }
    const handleOpenUpdateArticle = () => { setOpenUpdateArticleRow(true); };
    const handleOpenAddQuantite = () => {
        setOpenAddQuantite(!openAddQuantite);
        setOpenRemoveQuantite(false);
    };
    const handleOpenRemoveQuantite = () => {
        setOpenRemoveQuantite(!openRemoveQuantite);
        setOpenAddQuantite(false);
    };

    useEffect(() => { dispatch(getAllImagesAndArticles()) }, [dispatch, openUpdateArticleRow, openAddQuantite, openRemoveQuantite]);

    return (
        <React.Fragment>
            <TableRow >
                <TableCell sx={{ p: "9px", borderBottom: "none" }}>
                    {(value.imagesArticles).length > '' &&
                        <CardMedia
                            component="img"
                            image={`${urlImg + value.imagesArticles[0].image}`}
                            alt={value.imagesArticles[0].name}
                            sx={{ width: '100px', margin: 'auto' }}
                        />
                    }
                    {(value.imagesArticles).length <= '' &&
                        <CardMedia
                            component="img"
                            image={`${urlImg}/assets/images/noImage.jpg`}
                            alt="No_photography"
                            sx={{ width: '100px', margin: 'auto' }}
                        />
                    }
                </TableCell>
                <TableCell
                    sx={{
                        p: "5px", borderBottom: "none",
                        color: value.seuilCritique >= value.nombre ? 'red' : 'black'
                    }}>
                    {value.nom}
                </TableCell>
                <TableCell align='center'
                    sx={{
                        p: "5px", borderBottom: "none",
                        color: value.seuilCritique >= value.nombre ? 'red' : 'black'
                    }}>
                    {value.seuilCritique}
                </TableCell>
                <TableCell align='center'
                    sx={{
                        p: "5px", borderBottom: "none",
                        color: value.seuilCritique >= value.nombre ? 'red' : 'black'
                    }}>
                    {value.nombre}
                </TableCell>
                <TableCell align='center'
                    sx={{
                        p: "5px", borderBottom: "none",
                        color: value.seuilCritique >= value.nombre ? 'red' : 'black'
                    }}>
                    {value.prix}€
                </TableCell>
                <TableCell sx={{ p: "5px", borderBottom: "none" }}>
                    <Box sx={{ display: 'flex', justifyContent: 'space-around' }}>
                        <Button onClick={() => handleOpenUpdateArticle()} sx={{minWidth: "30px"}} >
                            <CreateIcon sx={{ color: 'grey' }} />
                        </Button>
                        <Button onClick={() => handleOpenAddQuantite()} sx={{minWidth: "30px"}} >
                            <AddIcon sx={{ color: 'grey' }} />
                        </Button>
                        <Button onClick={() => handleOpenRemoveQuantite()} sx={{minWidth: "30px"}} >
                            <RemoveIcon sx={{ color: 'grey' }} />
                        </Button>
                    </Box>
                </TableCell>
            </TableRow>
            <UpdateArticle
                value={value}
                openUpdateArticleRow={openUpdateArticleRow}
                setOpenUpdateArticleRow={setOpenUpdateArticleRow}
                handleClose={handleClose}
                dispatch={dispatch}
                NumericFormatCustom={NumericFormatCustom}
            />
            <TableRow>
                <TableCell style={{ paddingBottom: 0, paddingTop: 0 }} colSpan={6}>
                    <AddQuantite
                        value={value}
                        openAddQuantite={openAddQuantite}
                        setOpenAddQuantite={setOpenAddQuantite}
                        setOpenSuccessAdd={setOpenSuccessAdd}
                        NumericFormatCustom={NumericFormatCustom}
                        dispatch={dispatch}
                    />
                    <RemoveQuantite
                        value={value}
                        openRemoveQuantite={openRemoveQuantite}
                        setOpenRemoveQuantite={setOpenRemoveQuantite}
                        setOpenSuccessRemove={setOpenSuccessRemove}
                        NumericFormatCustom={NumericFormatCustom}
                        dispatch={dispatch}
                    />
                    <Snackbar open={openSuccessAdd} autoHideDuration={3000} onClose={handleCloseAlert} anchorOrigin={{ vertical: 'bottom', horizontal: 'right' }}>
                        <Alert severity="success" sx={{ width: '100%' }}>Quantité ajoutée avec succès!</Alert>
                    </Snackbar>
                    <Snackbar open={openSuccessRemove} autoHideDuration={3000} onClose={handleCloseAlert} anchorOrigin={{ vertical: 'bottom', horizontal: 'right' }}>
                        <Alert severity="success" sx={{ width: '100%' }}>Quantité déduite avec succès!</Alert>
                    </Snackbar>
                </TableCell>
            </TableRow>
        </React.Fragment >
    );
}