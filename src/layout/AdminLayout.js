import React from 'react';
import Container from '@mui/material/Container';
import CardMedia from '@mui/material/CardMedia';
import { styled, useTheme } from "@mui/material/styles";
import Box from "@mui/material/Box";
import Drawer from "@mui/material/Drawer";
import CssBaseline from "@mui/material/CssBaseline";
import Toolbar from "@mui/material/Toolbar";
import List from "@mui/material/List";
import ListItem from "@mui/material/ListItem";
import ListItemButton from "@mui/material/ListItemButton";
import ListItemIcon from "@mui/material/ListItemIcon";
import ListItemText from "@mui/material/ListItemText";
import LMULogo from "../assets/img/logo_LMU_Blanc.png";
import Typography from '@mui/material/Typography';
import LocalGroceryStoreIcon from '@mui/icons-material/LocalGroceryStore';

import { LocalizationProvider } from '@mui/x-date-pickers';
import { AdapterDayjs } from '@mui/x-date-pickers/AdapterDayjs'

import { useNavigate } from 'react-router-dom';

const drawerWidth = 280;

const Main = styled("main")(({ theme }) => ({ 
    flexGrow: 1, 
    padding: "64px 20px" ,
    marginLeft: "280px"
}));

function ListItemLine(props) {
    const { item } = props;
    const navigate = useNavigate();

    const handleNavigate = (item) => {
        navigate({ pathname: `${item.link}` })
    }

    return (
        <ListItem disablePadding onClick={(e) => handleNavigate(item)} >
            <ListItemButton>
                <ListItemText primary={item.titre} sx={{ ml: "60px", color: "#9DA4AE" }} />
            </ListItemButton>
        </ListItem>
    )
}

function AdminLayout({ children }) {
    const theme = useTheme();

    const MenuBoutiqueList = [
        { titre: "Gestion du stock", link: '/' },
        { titre: "Gestion des commandes", link: '/commande' }
    ];

    return (
        <Box>
            <CssBaseline />
            <Drawer
                variant="permanent"
                sx={{
                    display: { xs: 'none', sm: 'block' },
                    zIndex: "1200",
                    '& .MuiDrawer-paper': {
                        boxSizing: 'border-box',
                        bgcolor: '#1C2536',
                        width: drawerWidth
                    },
                }}
                open
            >
                <Toolbar>
                    <CardMedia
                        component="img"
                        image={LMULogo}
                        alt="logo-LMU"
                        sx={{ width: '150px', m: '40px auto 20px' }}
                    />
                </Toolbar>
                <Typography
                    variant='body'
                    sx={{ m: "20px 0 0 20px", fontWeight: 'bold', color: "#9DA4AE" }}
                >
                    <ListItemIcon sx={{ verticalAlign: "top" }}>
                        <LocalGroceryStoreIcon sx={{ color: "#9DA4AE" }} />
                    </ListItemIcon>
                    Boutique
                </Typography>
                <List>
                    {MenuBoutiqueList.map((item, index) => (
                        <ListItemLine key={index} item={item} />
                    ))}
                </List>
            </Drawer>
            <Main>
                <LocalizationProvider dateAdapter={AdapterDayjs}>
                    <Container>
                        {children}
                    </Container>
                </LocalizationProvider>
            </Main>
        </Box>
    )
}

export default AdminLayout
